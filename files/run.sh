#!/bin/bash
echo "Starting container..."
set -e  

if test $url; then 
	echo "OK: url is set to $url" 
else 
	echo "ERROR: 'url' env. variable is not set"
	exit
fi 

if test $dimension; then 
	echo "OK: dimension is set to $dimension" 
else 
	echo "ERROR: 'dimension' env. variable is not set (example '1024x768x24')"
	exit
fi 

if test $fps; then 
	echo "OK: fps is set to $fps" 
else 
	echo "ERROR: 'fps' env. variable is not set (example '30')"
	exit
fi 

if test $bitrate; then 
	echo "OK: bitrate is set to $bitrate" 
else 
	echo "ERROR: 'bitrate' env. variable is not set (example '5000')"
	exit
fi 

echo "Starting vlcast..."
su -c '/usr/local/bin/run_all.sh $url $dimension $fps $bitrate' vlcast
