#!/bin/bash
export DISPLAY=:1

#killall Xvfb
#sleep 0.5

rm /tmp/.X1-lock
rm -rf /home/vlcast/.config

/usr/bin/Xvfb :1 -screen 0 $2 -ac +extension GLX +render -noreset &
#/usr/bin/Xvfb :1 -screen 0 1280x720x24 -ac +extension GLX +render -noreset &
#killall x11vnc
sleep 1

DISPLAY=:1 dbus-launch
DISPLAY=:1 pulseaudio --start
#pactl load-module module-pipe-sink file=/tmp/audio
#sleep 0.5

#/usr/bin/x11vnc -display :1 -rfbport 9100 -forever &
#killall fluxbox
sleep 1

/usr/bin/fluxbox -display :1 &
sleep 0.5

/usr/bin/fbsetbg /opt/bg.jpg
#killall google-chrome
sleep 0.5

/usr/bin/google-chrome --test-type --no-first-run --disable-translate --no-sandbox -kiosk $1 &
#killall cvlc
sleep 3

/usr/bin/cvlc --input-slave pulse:// screen:// --screen-fps=$3 --nooverlay --sout "#transcode{vcodec=mp4v,vb=$4,scale=1,acodec=mp3,ab=256}:http{mux=ts,dst=:8088}"
